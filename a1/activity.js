let http = require("http");

//Mock Data
let products = [
	
   {
        name: "Iphone X",
        description: "Phone designed and created by Apple",
        price: 30000
    },
    {
        name: "Horizon Forbidden West",
        description: "Newest game for the PS4 and PS5",
        price: 4000
    },
    {
        name: "Razer Tiamat",
        description: "Headset from Razer",
        price: 3000
    }

];

http.createServer(function(request,response){
	
	console.log(request.url); //request URL endpoint
	console.log(request.method); //method of the request

	if(request.url === "/" && request.method === "GET"){

		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is a response to a GET method request");

	}else if(request.url === "/" && request.method === "POST"){

		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is a response to a POST method request");

	}else if(request.url === "/products" && request.method === "GET"){

		response.writeHead(201,{'Content-Type':'application/json'});
		response.end(JSON.stringify(products));

	}else if (request.url === "/products" && request.method === "POST"){

		// This route should be able to receive data from the client and we should be able to create a new product and add it to our courses array.

		//This will act as a placeholder to contain the data passed from the client.
		
		let requestBody = "";

		//Receiving data from a client to a nodejs server requires 2 steps:

		// data step - this part will read the stream of data from our client and process the incoming data into the requestBody variable.

		request.on('data',function(data){
			
			// console.log(data), // stram of data from client
			console.log(data); // data stream is saved into the variable
			requestBody += data;

		})

		// end step - this will run once or after the request data has been completely sent from our client.
		request.on('end',function(){

			console.log(requestBody);
			// requestBody now contains data from our Postman Client
			// since requestBody is JSON format we have to parse to add it as an object in our array.
			requestBody = JSON.parse(requestBody);

			// Simulate creating document and adding it in a collection:

			let newProducts = {
			
			// requestBody is now an object, we will get the name property of the requestBody as the value for the name of our newProducts object. Same with price.
			
				name: requestBody.name, 
				description:requestBody.description,
				price: requestBody.price
				
			}

			console.log(newProducts);
			products.push(newProducts);
			// check if the new product was added into the products array.
			console.log(products);
			
			response.writeHead(200,{'Content-Type':'application/json'});
			// send the updated products array to the client as a response
			response.end(JSON.stringify(products));
		})


		

	}}).listen(8000);

console.log("Server is running at localhost:8000");
